<?php

$title = $_POST['title'];
$ingredient0 = $_POST['ingredient0'];
$ingredient1 = $_POST['ingredient1'];
$ingredient2 = $_POST['ingredient2'];
$instructions = $_POST['instructions'];

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>CS295 Lab2</title>
        <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/default.css" rel="stylesheet" media="screen">
    </head>
    <body>                
        <div class="container">
        	<h1 class="title">Awesome recipe site</h1>

			<h2>Thank you for submitting <i><?php echo $title; ?></i></h2>
			
			<h2>Ingredients</h2>
			
			<ul>
				<li><?php echo $ingredient0; ?></li>
				<li><?php echo $ingredient1; ?></li>
				<li><?php echo $ingredient2; ?></li>
			</ul><br />
			<br />
			
			<h2>Instructions</h2>
			<p>
				<?php echo $instructions; ?>
			</p>
			
	        
		</div>
        
        <script src="http://code.jquery.com/jquery-latest.js"></script>
    	<script src="lib/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
